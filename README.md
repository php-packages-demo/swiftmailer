# [swiftmailer](https://phppackages.org/p/swiftmailer/swiftmailer)

[**swiftmailer/swiftmailer**](https://packagist.org/packages/swiftmailer/swiftmailer) [63](https://phppackages.org/s/swiftmailer) Comprehensive mailing tools for PHP https://swiftmailer.symfony.com/

* As of November 2021, Swiftmailer will not be maintained anymore. Use [Symfony Mailer](https://symfony.com/doc/current/mailer.html) instead. Read more on [Symfony's blog](https://symfony.com/doc/current/mailer.html).
* [Spool Swiftmailer emails to real message queue](https://blog.forma-pro.com/spool-swiftmailer-emails-to-real-message-queue-9ecb8b53b5de)
